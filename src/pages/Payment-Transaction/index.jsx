import Transaction from "@/components/AllTransaction";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import { Input } from "@/components/ui/input";
import QRTransactions from "@/components/QRTransactions";
import Credit from "@/components/Credit";
import Debit from "@/components/Debit";

const PaymentTransaction = () => {
  return (
    <>
      <Tabs defaultValue="transaction">
        <TabsList>
          <TabsTrigger value="transaction">Transaction</TabsTrigger>
          <TabsTrigger value="payment-received-via-qr">
            Payment Received Via QR Code
          </TabsTrigger>
          <TabsTrigger value="payment-received-via-link">
            Payment Received Via QR Link
          </TabsTrigger>
          <TabsTrigger value="debit">Debit</TabsTrigger>
        </TabsList>
        <TabsContent value="transaction">
          <Transaction />
        </TabsContent>
        <TabsContent value="payment-received-via-qr">
          <QRTransactions />
        </TabsContent>
        <TabsContent value="payment-received-via-link">
          <Credit />
        </TabsContent>
        <TabsContent value="debit">
          <Debit />
        </TabsContent>
      </Tabs>
    </>
  );
};

export default PaymentTransaction;
