import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Switch } from "@/components/ui/switch";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const initialValues = {
    userName: "admin@finnacle.com",
    password: "gi",
  };
  const [userlogin, setuserlogin] = useState("");
  const [passwordlogin, setuserpassword] = useState("");

  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const onChange = (checked) => {
    console.log(`switch to ${checked}`);
  };
  const navigate = useNavigate();

  const signIn = () => {

    if (
      userlogin == initialValues.userName &&
      passwordlogin == initialValues.password
    ) {
      navigate("/dashboard");
    } else if (userlogin == "" && passwordlogin == "") {
      alert("Please Enter User Name And Password");
    } else if (
      userlogin != initialValues.userName ||
      passwordlogin != initialValues.password
    ) {
      alert("Invalid  User Name or Password");
    }
  };

  return (
    <>
      <div className="flex">
        <div className="w-[60%] relative bg-primary h-screen">
          {/* <div className="h-screen w-full absolute inset-0 bg-primary"></div> */}
          <img src="./login.png" alt="bank" className="w-[60%] mx-auto" />
        </div>
        <div className="w-[40%] flex items-center justify-center p-12">
          <div className="w-full">
            <h1 className="text-primary font-bold text-3xl my-4">Login</h1>
            <div className="my-4">
              <label htmlFor="" className="text-sm text-gray-600">
                Email
              </label>
              <Input
                value={userlogin}
                placeholder="johndoe@example.com"
                onChange={(e) => setuserlogin(e.target.value)}
              />
            </div>
            <div className="my-4">
              <label htmlFor="" className="text-sm text-gray-600">
                Password
              </label>
              <Input
                value={passwordlogin}
                type="password"
                placeholder="johndoe@example.com"
                onChange={(e) => setuserpassword(e.target.value)}
              />
            </div>
            <div className="my-4 flex items-center space-x-2">
              <Switch />
              <label htmlFor="" className="text-sm text-gray-600">
                Remember me
              </label>
            </div>
            <Button onClick={signIn} className="w-full">Login</Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
