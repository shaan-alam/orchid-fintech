import ManageQrList from "@/components/ManageQrList";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { useState } from "react";
import PaymentLink from "@/components/PaymentLink";
import ManagePaymentButton from "@/components/ManagePaymentButton";



const PaymentProduct = () => {
  const [state, setState] = useState(false);

  const [bank, setBank] = useState("");
  const [gate, setGate] = useState("");
  const [payment, setPayment] = useState("");

  const fetchMethod = () => {
    setState(true);
  };

  const bankNames = [
    { value: "AXIS BANK", label: "AXIS BANK" },
    { value: "HDFC BANK", label: "HDFC BANK" },
    { value: "ICICI BANK", label: "ICICI BANK" },
    { value: "SBI BANK", label: "SBI BANK" },
  ];
  const gateway = [
    { value: "Razor Pay", label: "Razor Pay" },
    { value: "PayU Money", label: "PayU Money" },
    { value: "Paytm", label: "Paytm" },
    { value: "Google Pay", label: "Google Pay" },
  ];
  const paymentOption = [
    { value: "QR Code", label: "QR Code" },
    { value: "Payment Link", label: "Payment Link" },
    { value: "Payment Button", label: "Payment Button" },
  ];

  return (
    <>
      <div className="my-6 grid grid-cols-3 gap-x-4 bg-white rounded-xl shadow p-6">
        <div>
          <label htmlFor="" className="text-sm text-gray-600">
            Select Bank
          </label>
          <Select value={bank} onValueChange={(value) => setBank(value)}>
            <SelectTrigger>
              <SelectValue placeholder="Bank" />
            </SelectTrigger>
            <SelectContent>
              {bankNames.map((bank) => (
                <SelectItem value={bank.value}>{bank.label}</SelectItem>
              ))}
            </SelectContent>
          </Select>
        </div>
        <div>
          <label htmlFor="" className="text-sm text-gray-600">
            Select Gateway
          </label>
          <Select value={gate} onValueChange={(value) => setGate(value)}>
            <SelectTrigger>
              <SelectValue placeholder="Payment Gateways" />
            </SelectTrigger>
            <SelectContent>
              {gateway.map((gateway) => (
                <SelectItem value={gateway.value}>{gateway.label}</SelectItem>
              ))}
            </SelectContent>
          </Select>
        </div>
        <div>
          <label htmlFor="" className="text-sm text-gray-600">
            Select Payment Option
          </label>
          <Select value={payment} onValueChange={(value) => setPayment(value)}>
            <SelectTrigger>
              <SelectValue placeholder="Payment Option" />
            </SelectTrigger>
            <SelectContent>
              {paymentOption.map((option) => (
                <SelectItem value={option.value}>{option.label}</SelectItem>
              ))}
            </SelectContent>
          </Select>
        </div>
        <div>
          <Button onClick={(e) => fetchMethod()} className="mt-6">
            Submit
          </Button>
        </div>
      </div>
      {state ? (
        <>
          {(() => {
            if (
              bank == "AXIS BANK" &&
              gate == "Razor Pay" &&
              payment == "QR Code"
            ) {
              return (
                <>
                  <ManageQrList></ManageQrList>
                </>
              );
            } else if (
              bank == "AXIS BANK" &&
              gate == "Razor Pay" &&
              payment == "Payment Link"
            ) {
              return <PaymentLink></PaymentLink>;
            } else if (
              bank == "AXIS BANK" &&
              gate == "Razor Pay" &&
              payment == "Payment Button"
            ) {
              return <ManagePaymentButton></ManagePaymentButton>;
            } else {
              return (
                <>
                  <h3 style={{ textAlign: "center", color: "red" }}>
                    Payment Gateway is not Connected
                  </h3>
                </>
              );
            }
          })()}{" "}
        </>
      ) : (
        ""
      )}
    </>
  );
};

export default PaymentProduct;
