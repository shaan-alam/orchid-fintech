import Dashboard from "@/components/Dashboard";
import Gateway from "@/components/Gatway";
import Login from "@/pages/Login";
import PaymentTransaction from "@/pages/Payment-Transaction";
import PaymentProduct from "@/pages/PaymentProduct";

export const publicRoutes = [{ path: "/", component: <Login /> }];

export const privateRoutes = [
  { path: "/dashboard", component: <Dashboard /> },
  {
    path: "/payment-product",
    component: <PaymentProduct />,
  },
  {
    path: "/payment-transaction",
    component: <PaymentTransaction />,
  },
  {
    path: '/gateway',
    component: <Gateway />
  },
  
];
