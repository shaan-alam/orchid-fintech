import { BrowserRouter, Route, Routes } from "react-router-dom";
import { publicRoutes, privateRoutes } from "@/routes";
import Layout from "./components/Layout";
import Login from "./pages/Login";
import './App.css';

const App = () => {
  return (
    <>
      <BrowserRouter>
        <Routes>
          {publicRoutes.map((item, index) => {
            return (
              <Route key={index} path={item.path} element={item.component} />
            );
          })}
          {privateRoutes.map((item, index) => {
            return (
              <Route
                key={index}
                path={item.path}
                element={<Layout>{item.component}</Layout>}
              />
            );
          })}
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default App;
