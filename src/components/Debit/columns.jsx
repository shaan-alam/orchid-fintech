import EditOutlined from "@ant-design/icons/EditOutlined";
import SettlementDialog from "./SettlementDialog";

export const columns = [
  {
    accessorKey: "key",
    header: "Sr No",
  },
  {
    accessorKey: "brmanager",
    header: "Branch Manager",
  },
  {
    accessorKey: "branchLocation",
    header: "Branch Location",
  },
  {
    header: "Bank Name",
    accessorKey: "bankName",
  },
  {
    header: "Gateway",
    accessorKey: "gateway",
  },
  {
    header: "Amount",
    accessorKey: "amount",
  },
  {
    header: "Gateway Charges",
    accessorKey: "gatewayCharges",
  },
  {
    header: "Commission",
    accessorKey: "commission",
  },
  {
    header: "Settlement",
    accessorKey: "settlement",
  },
  {
    header: "Actions",
    accessorKey: "actions",
    cell: () => {
      return (
        <>
          <SettlementDialog />
        </>
      );
    },
  },
];

export const data = [
  {
    key: "1",
    brmanager: "Vinod",
    branchLocation: "Thane",
    bankName: "ICICI",
    gateway: "Razor Pay",
    amount: "₹ 4,800",
    gatewayCharges: "1.8 %",
    date: "02-02-2023",
    commission: "0.5%",
    settlement: "₹ 4,689.6",
  },
  {
    key: "T4251003255",
    brmanager: "CR",
    branchLocation: "₹ 2,400",
    bankName: "Paid",
    gateway: "Paid",
    amount: "Paid",
    gatewayCharges: "Paid",
    date: "12-12-2022",
    commission: "0.5%",
    settlement: "₹ 11,940",
  },
];
