import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger
} from "@/components/ui/dialog";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Badge } from "../ui/badge";
import { Input } from "../ui/input";

const SettlementDialog = () => {
  const manager = [
    { value: "Vinod", label: "Vinod" },
    { value: "Rajeshwar", label: "Rajeshwar" },
    { value: "Vishal", label: "Vishal" },
  ];
  const location = [
    { value: "Thane", label: "Thane" },
    { value: "Juhu", label: "Juhu" },
    { value: "Kalyan", label: "Kalyan" },
  ];
  const bankName = [
    { value: "ICICI", label: "ICICI" },
    { value: "SBI", label: "SBI" },
    { value: "HDFC", label: "HDFC" },
    { value: "Kotak", label: "Kotak" },
  ];
  const dateFormat = "YYYY-MM-DD";
  const modes = [
    { value: "Cash", label: "Cash" },
    { value: "Cheque", label: "Cheque" },
    { value: "Debit Card", label: "Debit Card" },
  ];

  return (
    <>
      <Dialog>
        <DialogTrigger>
          <Badge>Settlement</Badge>
        </DialogTrigger>
        <DialogContent>
          <DialogHeader>
            <DialogTitle>Settlement</DialogTitle>
          </DialogHeader>
          <div className="grid grid-cols-2 gap-4">
            <div>
              <label htmlFor="" className="text-sm text-gray-600">
                Bank Name
              </label>
              <Select>
                <SelectTrigger>
                  <SelectValue placeholder="Bank Name" />
                </SelectTrigger>
                <SelectContent>
                  {bankName.map((bank) => (
                    <SelectItem value={bank.value}>{bank.label}</SelectItem>
                  ))}
                </SelectContent>
              </Select>
            </div>
            <div>
              <label htmlFor="" className="text-sm text-gray-600">
                Payment Mode
              </label>
              <Select>
                <SelectTrigger>
                  <SelectValue placeholder="Payment Mode" />
                </SelectTrigger>
                <SelectContent>
                  {modes.map((mode) => (
                    <SelectItem value={mode.value}>{mode.label}</SelectItem>
                  ))}
                </SelectContent>
              </Select>
            </div>
            <div>
              <label htmlFor="" className="text-sm text-gray-600">
                Amount
              </label>
              <Input placeholder="Enter the Amount" />
            </div>
          </div>
          <DialogFooter>
            <DialogClose>
              <Button variant="secondary">Close</Button>
            </DialogClose>
            <Button>Transfer</Button>
          </DialogFooter>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default SettlementDialog;
