import { Input } from "@/components/ui/input";
import AddDebit from "../AddDebit";
import { DataTable } from "@/components/ui/data-table";
import { columns, data } from './columns'

const Debit = () => {
  return (
    <>
      <div className="flex items-center space-x-2 bg-white rounded-xl shadow my-4 p-4">
        <Input placeholder="Search" />
        <AddDebit />
      </div>
      <div className="bg-white rounded-xl shadow my-4 p-4">
        <h1 className="text-primary font-semibold text-lg">Debits</h1>
        <div className="my-4">
          <DataTable columns={columns} data={data} />
        </div>
      </div>
    </>
  );
};

export default Debit;
