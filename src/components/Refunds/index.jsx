import React from "react";
import { Table } from "antd";

const Refunds = () => {
  const columns = [
    {
      title: "Refund ID",
      dataIndex: "refundId",
      key: "refundId",
    },
    {
      title: "Payment ID",
      dataIndex: "paymentId",
      key: "paymentId",
    },
    {
      title: "Amount",
      dataIndex: "amount",
      key: "amount",
    },
    {
      title: "Created At",
      dataIndex: "createdAt",
      key: "createdAt",
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
    },
  ];

  const data = [];

  return (
    <div className="my-4">
      <Table columns={columns} dataSource={data} />
    </div>
  );
};

export default Refunds;
