import Sidebar from "../Sidebar";
import Topbar from "../Topbar";

const Layout = ({ children }) => {
  return (
    <>
      <div className="flex">
        <div className="w-[15%]">
          <Sidebar />
        </div>
        <div className="w-[85%] p-4">
          <Topbar />
          {children}
        </div>
      </div>
    </>
  );
};

export default Layout;
