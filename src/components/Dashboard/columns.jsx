export const columns = [
  {
    accessorKey: "transactionNumber",
    header: "Transaction Number",
  },
  {
    accessorKey: "paymentType",
    header: "Payment Type",
  },
  {
    accessorKey: "amount",
    header: "Amount",
  },
  {
    accessorKey: "status",
    header: "Status",
  },
];

export const data = [
  {
    transactionNumber: "T4251003255",
    paymentType: "CR",
    amount: "₹ 2,400",
    status: "Paid",
  },
  {
    transactionNumber: "T0789541122",
    paymentType: "DB",
    amount: "	₹ 900",
    status: "In Progress",
  },
];
