import { Card, CardContent, CardHeader, CardTitle } from "@/components/ui/card";
import { CartIcon, DollarIcon, ProfileIcon } from "@/icons";
import ReactApexChart from "react-apexcharts";
import EChart from "../Echart";
import LineChart from "../LineChart";
import { DataTable } from "../ui/data-table";
import { columns, data } from "./columns";
import { Progress } from "../ui/progress";
import { cn } from "@/lib/utils";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "../ui/tabs";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { DownloadIcon } from "@radix-ui/react-icons";
import { Button } from "../ui/button";
import Payments from "@/components/Payments";
import Refunds from "../Refunds";
import Settlements from "../Settlements";

const Dashboard = () => {
  const state = {
    series: [50000, 15000, 10000, 5000],
    options: {
      chart: {
        width: 380,
        type: "pie",
      },
      labels: ["Salary", "Freelancing", "Investments", "Other"],
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200,
            },
            legend: {
              position: "bottom",
            },
          },
        },
      ],
    },
  };

  const count = [
    {
      today: "Total Credit",
      title: "₹ 53,000",
      persent: "+30%",
      icon: DollarIcon,
      bnb: "bnb2",
      color: "bg-[#A200C3]",
    },
    {
      today: "Total Debit",
      title: "₹ 3,200",
      persent: "+20%",
      icon: ProfileIcon,
      bnb: "bnb2",
      color: "bg-[#683DA2]",
    },
    {
      today: "Total Commission",
      title: "₹ 13,200",
      persent: "10%",
      icon: CartIcon,
      bnb: "bnb2",
      color: "bg-[#4476D7]",
    },
    {
      today: "Gateway Amount",
      title: "₹ 49,560",
      persent: "10%",
      icon: CartIcon,
      bnb: "bnb2",
      color: "bg-[#D33132]",
    },
  ];

  const count1 = [
    {
      today: "Total Branch Manager",
      title: "5",
      persent: "10%",
      icon: CartIcon,
      bnb: "bnb2",
      color: "bg-[#A200C3]",
    },
    {
      today: "Gateway Charges",
      title: "₹ 1,250",
      persent: "10%",
      icon: CartIcon,
      bnb: "bnb2",
      color: "bg-[#683DA2]",
    },
    {
      today: "Received Commission",
      title: "₹ 8,250",
      persent: "10%",
      icon: CartIcon,
      bnb: "bnb2",
      color: "bg-[#4476D7]",
    },
    {
      today: "Pending Commission",
      title: "₹ 2,400",
      persent: "10%",
      icon: CartIcon,
      bnb: "bnb2",
      color: "bg-[#D33132]",
    },
  ];

  const count2 = [
    {
      today: "Settlement Cleared",
      title: "₹ 5,200",
      persent: "10%",
      icon: CartIcon,
      bnb: "bnb2",
      color: "bg-[#A200C3]",
    },
    {
      today: "Settlement Pending",
      title: "₹ 6,000",
      persent: "10%",
      icon: CartIcon,
      bnb: "bnb2",
      color: "bg-[#683DA2]",
    },
    {
      today: "Total Settlement",
      title: "₹ 8,400",
      persent: "10%",
      icon: CartIcon,
      bnb: "bnb2",
      color: "bg-[#4476D7]",
    },
  ];

  const chartData = {
    series: [
      {
        name: "Value",
        data: [38, 52, 61, 145, 48, 38, 38, 38],
      },
    ],
    options: {
      chart: {
        type: "bar",
        height: 350,
      },
      plotOptions: {
        bar: {
          borderRadius: 4,
          horizontal: false,
        },
      },
      dataLabels: {
        enabled: false,
      },
      xaxis: {
        categories: [
          "Category A",
          "Category B",
          "Category C",
          "Category D",
          "Category E",
          "Category F",
          "Category G",
          "Category H",
        ],
      },
      yaxis: {
        title: {
          text: "Value",
        },
      },
      fill: {
        opacity: 1,
      },
      tooltip: {
        y: {
          formatter: (val) => `${val}`,
        },
      },
    },
  };

  return (
    <div>
      <div className="my-4">
        <Card>
          <CardContent>
            <div className="grid grid-cols-3 gap-4">
              <div className="p-4 border-r">
                <h1 className="text-[#D84349] font-medium text-2xl mb-2">
                  Payment Volume
                </h1>
                <p className="text-lg text-gray-500">₹100</p>
              </div>
              <div className="p-4 border-r">
                <h1 className="text-[#D84349] font-medium text-2xl mb-2">
                  No of Payments
                </h1>
                <p className="text-lg text-gray-500">₹100</p>
              </div>
              <div className="p-4">
                <h1 className="text-[#D84349] font-medium text-2xl mb-2">
                  No of Refunds
                </h1>
                <p className="text-lg text-gray-500">₹100</p>
              </div>
            </div>
          </CardContent>
        </Card>
      </div>
      <div className="my-4 rounded-xl bg-white p-4 shadow">
        <div className="flex items-center justify-end space-x-2">
          <Tabs defaultValue="daily">
            <TabsList>
              <TabsTrigger value="daily">Daily</TabsTrigger>
              <TabsTrigger value="weekly">Weekly</TabsTrigger>
              <TabsTrigger value="monthly">Monthly</TabsTrigger>
            </TabsList>
          </Tabs>
          <Select defaultValue="by-total-volume">
            <SelectTrigger className="w-[180px]">
              <SelectValue placeholder="" />
            </SelectTrigger>
            <SelectContent>
              <SelectItem value="by-total-volume">By Total Volume</SelectItem>
            </SelectContent>
          </Select>
          <Button variant="secondary">
            <DownloadIcon />
          </Button>
        </div>
        <ReactApexChart
          options={chartData.options}
          series={chartData.series}
          type="bar"
          height={350}
        />
      </div>
      <div className="my-4 rounded-xl bg-white shadow p-4">
        <h1 className="text-primary font-medium text-lg">Recent Activity</h1>
        <Tabs className="my-4" defaultValue="payments">
          <TabsList className="w-full">
            <TabsTrigger value="payments" className="w-full">
              Payments
            </TabsTrigger>
            <TabsTrigger value="settlements" className="w-full">
              Settlements
            </TabsTrigger>
            <TabsTrigger value="refunds" className="w-full">
              Refunds
            </TabsTrigger>
          </TabsList>
          <TabsContent value="payments">
            <Payments />
          </TabsContent>
          <TabsContent value="settlements">
            <Settlements />
          </TabsContent>
          <TabsContent value="refunds">
            <Refunds />
          </TabsContent>
        </Tabs>
      </div>
      <div className="grid grid-cols-4 gap-4">
        {count.map((item, index) => (
          <Card
            className={cn(
              "transition-all hover:scale-105 hover:shadow-md",
              item.color
            )}
          >
            <CardContent className="p-4">
              <div>
                <div className="flex items-center justify-between">
                  <div className="flex items-center space-x-2">
                    <span>
                      <item.icon />
                    </span>
                    <h3 className="text-sm text-white font-semibold mb-2">
                      {item.today}
                    </h3>
                  </div>
                  <h1 className="font-bold text-xl text-white">{item.title}</h1>
                </div>
                <div className="mt-3">
                  <Progress value={40} />
                </div>
              </div>
            </CardContent>
          </Card>
        ))}
      </div>
      <div className="grid grid-cols-4 gap-4 my-4">
        {count1.map((item, index) => (
          <Card
            className={cn(
              "transition-all hover:scale-105 hover:shadow-md",
              item.color
            )}
          >
            <CardContent className="p-4">
              <div>
                <div className="flex items-center justify-between">
                  <div className="flex items-center space-x-2">
                    <span>
                      <item.icon />
                    </span>
                    <h3 className="text-sm text-white font-semibold mb-2">
                      {item.today}
                    </h3>
                  </div>
                  <h1 className="font-bold text-xl text-white">{item.title}</h1>
                </div>
                <div className="mt-3">
                  <Progress value={60} />
                </div>
              </div>
            </CardContent>
          </Card>
        ))}
      </div>
      <div className="grid grid-cols-3 gap-4 mt-12 mb-8">
        {count2.map((item, index) => (
          <Card
            className={cn(
              "transition-all hover:scale-105 hover:shadow-md",
              item.color
            )}
          >
            <CardContent className="p-4">
              <div>
                <div className="flex items-center justify-between">
                  <div className="flex items-center space-x-2">
                    <span>
                      <item.icon />
                    </span>
                    <h3 className="text-sm text-white font-semibold mb-2">
                      {item.today}
                    </h3>
                  </div>
                  <h1 className="font-bold text-xl text-white">{item.title}</h1>
                </div>
                <div className="mt-3">
                  <Progress value={70} />
                </div>
              </div>
            </CardContent>
          </Card>
        ))}
      </div>
      <div className="w-full">
        <Card>
          <CardHeader>
            <CardTitle>Recent Payment Activity</CardTitle>
          </CardHeader>
          <CardContent>
            <div className="w-full">
              <DataTable columns={columns} data={data} />
            </div>
          </CardContent>
        </Card>
      </div>
      <div className="grid grid-cols-2 gap-4 mt-4">
        <Card>
          <CardHeader>
            <CardTitle>Credits</CardTitle>
          </CardHeader>
          <CardContent className="p-4">
            <EChart />
          </CardContent>
        </Card>
        <Card>
          <CardHeader>
            <CardTitle>Debits</CardTitle>
          </CardHeader>
          <CardContent className="p-4">
            <LineChart />
          </CardContent>
        </Card>
        <Card>
          <CardHeader>
            <CardTitle>Debits</CardTitle>
          </CardHeader>
          <CardContent className="p-4">
            <ReactApexChart
              options={state.options}
              series={state.series}
              type="pie"
              width={380}
            />
          </CardContent>
        </Card>
      </div>
    </div>
  );
};

export default Dashboard;
