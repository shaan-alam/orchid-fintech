import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Button } from "../ui/button";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Input } from '@/components/ui/input';

const CreateCreditDialog = () => {
  const manager = [
    { value: "Vinod", label: "Vinod" },
    { value: "Rajeshwar", label: "Rajeshwar" },
    { value: "Vishal", label: "Vishal" },
  ];
  const location = [
    { value: "Thane", label: "Thane" },
    { value: "Juhu", label: "Juhu" },
    { value: "Kalyan", label: "Kalyan" },
  ];
  const bankName = [
    { value: "ICICI", label: "ICICI" },
    { value: "SBI", label: "SBI" },
    { value: "HDFC", label: "HDFC" },
    { value: "Kotak", label: "Kotak" },
  ];
  const dateFormat = "YYYY-MM-DD";
  const modes = [
    { value: "Bank Transfer", label: "Bank Transfer" },
    { value: "Payemnt Gateway", label: "Payemnt Gateway" },
  ];
  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <Dialog>
      <DialogTrigger>
        <Button>Create Credit</Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Create Credit</DialogTitle>
        </DialogHeader>
        <div className="grid grid-cols-2 gap-4">
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Branch Manager
            </label>
            <Select>
              <SelectTrigger>
                <SelectValue placeholder="Branch Manager" />
              </SelectTrigger>
              <SelectContent>
                {manager.map((manager) => (
                  <SelectItem value={manager.value}>{manager.label}</SelectItem>
                ))}
              </SelectContent>
            </Select>
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Branch Location
            </label>
            <Select>
              <SelectTrigger>
                <SelectValue placeholder="Branch Location" />
              </SelectTrigger>
              <SelectContent>
                {location.map((location) => (
                  <SelectItem value={location.value}>
                    {location.label}
                  </SelectItem>
                ))}
              </SelectContent>
            </Select>
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Bank Name
            </label>
            <Select>
              <SelectTrigger>
                <SelectValue placeholder="Bank Name" />
              </SelectTrigger>
              <SelectContent>
                {bankName.map((bank) => (
                  <SelectItem value={bank.value}>{bank.label}</SelectItem>
                ))}
              </SelectContent>
            </Select>
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Payment Gateway
            </label>
            <Select>
              <SelectTrigger>
                <SelectValue placeholder="Bank Name" />
              </SelectTrigger>
              <SelectContent>
                {modes.map((gateway) => (
                  <SelectItem value={gateway.value}>{gateway.label}</SelectItem>
                ))}
              </SelectContent>
            </Select>
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Total Amount
            </label>
            <Input placeholder="Total Amount" />
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Gateway Charges
            </label>
            <Input placeholder="Gateway Charges" />
          </div>
        </div>
        <DialogFooter>
          <DialogClose>
            <Button variant="secondary">Close</Button>
          </DialogClose>
          <Button>Add</Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default CreateCreditDialog;
