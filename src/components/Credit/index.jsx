import { DataTable } from "../ui/data-table";
import { Input } from "../ui/input";
import CreateCreditDialog from "./CreateCreditDialog";
import { columns, data } from "./columns";

const Credit = () => {
  return (
    <>
      <div className="rounded-xl bg-white p-4 shadow flex items-center space-x-2 my-4">
        <Input placeholder="Search" />
        <CreateCreditDialog />
      </div>
      <div className="rounded-xl bg-white p-4 shadow">
        <h1 className="text-primary font-semibold text-lg">Credits</h1>
        <div className="my-4">
          <DataTable columns={columns} data={data} />
        </div>
      </div>
    </>
  );
};

export default Credit;
