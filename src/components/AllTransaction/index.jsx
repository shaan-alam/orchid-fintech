import { Input } from "@/components/ui/input";
import { DataTable } from "@/components/ui/data-table";
import { columns } from "./columns";
import { useState, useEffect } from "react";

const Transaction = () => {
  const [APIData, setAPIData] = useState([]);

  const [obj, setObj] = useState(null);

  const getData = async () => {
    await Axios.get(BASE_URL + "/payments").then((res) => {
      setAPIData(
        res.data.items.map((row) => ({
          id: <a onClick={() => showModal(row)}>{row.id}</a>,
          // date: new Date(row.created_at * 1000).toLocaleDateString("en-GB"),
          currency: row.currency,
          date: (
            <span>
              {new Date(row.created_at * 1000).toLocaleDateString("en-GB")}
            </span>
          ),
          amount: (
            <>
              {row.currency == "USD" ? <>$ </> : <> ₹ </>}
              <span>{row.amount / 100}</span>
            </>
          ),
          method: row.method,
          wallet: row.wallet,
          email: row.email,
          contact: row.contact,
          status: (
            <>
              {row.status == "captured" ? (
                <>
                  <Chip size="small" label="Captured" color="success" />
                </>
              ) : (
                <>
                  {" "}
                  <Chip size="small" label="Failed" color="warning" />
                </>
              )}
            </>
          ),
          // action: (
          //   <>
          //     <IconButton onClick={() => handleshow(row)}>
          //       <EyeOutlined style={{ fontSize: "18px" }}></EyeOutlined>
          //     </IconButton>
          //   </>
          // ),
        }))
      );
    });
  };
  const showModal = (record) => {
    setObj(record);
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    getData();
    setIsModalOpen(false);
  };
  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <div className="rounded-xl bg-white p-6 shadow my-4">
        <Input placeholder="Search" className="my-4" />
        <DataTable columns={columns} data={[]} />
      </div>
    </>
  );
};

export default Transaction;
