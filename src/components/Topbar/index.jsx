import { Input } from "@/components/ui/input";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import { cn } from "@/lib/utils";
import { PersonIcon } from "@radix-ui/react-icons";
import { Link } from "react-router-dom";
import { HiViewGrid } from "react-icons/hi";
import { AiFillNotification } from "react-icons/ai";
import { MdMonitorHeart } from "react-icons/md";
import { LuX } from "react-icons/lu";

const Topbar = () => {
  return (
    <>
      <div className="flex items-center justify-between mb-6 border-b pb-6 py-4">
        <div>
          <Input placeholder="Search" />
        </div>
        <div>
          <div className="flex items-center space-x-4">
            <span className="p-2 rounded-full hover:bg-gray-100 cursor-pointer">
              <AiFillNotification size={20} className="text-gray-500" />
            </span>
            <span className="p-2 rounded-full hover:bg-gray-100 cursor-pointer">
              <MdMonitorHeart size={20} className="text-gray-500" />
            </span>
            <span className="p-2 rounded-full hover:bg-gray-100 cursor-pointer">
              <HiViewGrid size={20} className="text-gray-500" />
            </span>
            <Popover>
              <PopoverTrigger>
                <PersonIcon className="h-[35px] w-[35px] text-[#333] p-2 rounded-full hover:bg-accent" />
              </PopoverTrigger>
              <PopoverContent>
                <ul>
                  <Link to="/">
                    <li
                      className={cn(
                        "flex items-center space-x-4 px-4 py-1 cursor-pointer hover:bg-secondary rounded-lg"
                      )}
                    >
                      Profile
                    </li>
                  </Link>
                  <Link to="/">
                    <li
                      className={cn(
                        "flex items-center space-x-4 px-4 py-1 cursor-pointer hover:bg-secondary rounded-lg"
                      )}
                    >
                      Logout
                    </li>
                  </Link>
                </ul>
              </PopoverContent>
            </Popover>
          </div>
        </div>
      </div>
      <div className="flex items-center rounded-md mb-4 shadow bg-white">
        <div className="bg-green-600 text-white font-semibold p-4 rounded-md text-sm w-fit text-nowrap">
          Account Activated
        </div>
        <div className="bg-white p-4 text-gray-500 text-sm rounded-tr-md rounded-br-md flex items-center justify-between w-full">
          <p>
            Congratulations! Your KYC has been successfully verified and your
            account has been activated. Payments recevied by you willl be
            settled to your account as per your settlement schedule
          </p>
          <LuX size={25} className="cursor-pointer"/>
        </div>
      </div>
      <div className="flex items-center rounded-md mb-4 shadow bg-white">
        <div className="bg-purple-600 text-white font-semibold p-4 rounded-md text-sm w-fit text-nowrap">
          Account Activated
        </div>
        <div className="bg-white p-4 text-gray-500 text-sm rounded-tr-md rounded-br-md flex items-center justify-between w-full">
          <p>
            You can now effortlessly setup email schedules for your reports
            within a few clicks.
          </p>
          <LuX size={25} className="cursor-pointer"/>
        </div>
      </div>
    </>
  );
};

export default Topbar;
