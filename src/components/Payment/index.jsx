import googlepay from "@/assets/google-pay.png";
import paytm from "@/assets/paytm.png";
import razorpay from "@/assets/razorpay.jpg";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { Card, Col, Row, message } from "antd";
import { useState } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import ShareGatewayDialog from "./ShareGatewayDialog";

const Payment = () => {
  const [copied, setCopied] = useState(false);
  const [showLink, setShowLink] = useState(false);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [showForm, setShowForm] = useState(false);
  const [filterPlatforms, setFilterPlatforms] = useState([]);
  const [notConnectdPlatforms, setNotConnectedPlatforms] = useState([]);
  const AccountNo = [
    { value: "65645656", label: "65645656" },
    { value: "56767676", label: "56767676" },
    { value: "5675675676", label: "5675675676" },
  ];

  const p = ["paytm", "razorpay", "googlepay"];

  const platformNames = [
    { value: "paytm", label: "paytm" },
    { value: "googlepay", label: "googlepay" },
    { value: "razorpay", label: "razorpay" },
  ];

  const bankPlatforms = [
    {
      value: "SBI",
      platforms: [
        // { image: paytm, link: "http://paytm/87452145@paytm", name: "paytm" },
        {
          image: razorpay,
          link: "https://rzp.io/i/vOCA2rx2d3",
          name: "razorpay",
        },
      ],
    },
    {
      value: "ICICI",
      platforms: [
        {
          image: razorpay,
          link: "http://razorpay/vinos784@paytm",
          name: "razorpay",
        },
        { image: paytm, link: "http://paytm/87452145@paytm", name: "paytm" },
      ],
    },
    {
      value: "Bank Of Maharashtra",
      platforms: [
        {
          image: googlepay,
          link: "http://googlepay/785@axis",
          name: "googlepay",
        },
      ],
    },
  ];

  const bankNames = [
    { value: "SBI", label: "SBI" },
    { value: "ICICI", label: "ICICI" },
    { value: "Bank Of Maharashtra", label: "Bank Of Maharashtra" },
  ];
  const onChange = (e) => {
    console.log(`checked = ${e.target.checked}`);
  };
  const onChange1 = (e) => {
    console.log(`checked = ${e.target.checked}`);
  };
  const getPlatforms = (bankName) => {
    var arr = bankPlatforms.filter((x) => x.value == bankName);
    setFilterPlatforms(arr[0].platforms);
    var names = [];
    for (var i = 0; i < arr[0].platforms.length; i++) {
      names.push(arr[0].platforms[i].name);
    }
    var filter1 = p.filter((x) => !names.includes(x));
    var notCnnPlat = [];
    for (var i = 0; i < filter1.length; i++) {
      if (filter1[i] == "googlepay") {
        notCnnPlat.push({ image: googlepay });
      }
      if (filter1[i] == "paytm") {
        notCnnPlat.push({ image: paytm });
      }
      if (filter1[i] == "razorpay") {
        notCnnPlat.push({ image: razorpay });
      }
    }
    setNotConnectedPlatforms(notCnnPlat);
  };
  const showModal1 = () => {
    setShowForm(true);
  };

  const handleOpen = () => {
    setShowForm(false);
  };

  const handleClose = () => {
    setShowForm(false);
  };
  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const [messageApi] = message.useMessage();
  const success = () => {
    messageApi.open({
      type: "success",
      content: "This is a success message",
    });
  };
  const genrateLink = () => {
    alert("Payment Link Generated Successfully");
    setShowLink(true);
  };
  const sendLink = () => {
    handleClose();
    alert("Link Send Successfully!");
  };

  return (
    <>
      <div className="bg-white rounded-xl p-4 shadow mt-4">
        <label htmlFor="" className="text-sm text-gray-600">
          Payment Gateways
        </label>
        <div className="flex items-center space-x-2">
          <Select onValueChange={(value) => getPlatforms(value)}>
            <SelectTrigger className="w-[200px]">
              <SelectValue placeholder="Payment Gateways" />
            </SelectTrigger>
            <SelectContent>
              {bankNames.map((bank) => (
                <SelectItem value={bank.value}>{bank.label}</SelectItem>
              ))}
            </SelectContent>
          </Select>
        </div>
      </div>
      {filterPlatforms.map((item) => {
        return (
          <Row gutter={[24, 0]} style={{ marginTop: "20px" }}>
            <Col span={24} md={24}>
              <Card className="payment-method-card">
                <img
                  src={item.image}
                  alt="paytm"
                  style={{ height: "50px", width: "70px" }}
                />
                {!showLink ? (
                  <Input
                    className="card-number"
                    placeholder="Please Generate Link"
                    style={{ marginBottom: "8px" }}
                  ></Input>
                ) : (
                  <Input className="card-number" value={item.link}></Input>
                )}

                {!showLink ? (
                  <>
                    <Button type="primary" onClick={genrateLink}>
                      Generate
                    </Button>
                    &nbsp;
                  </>
                ) : (
                  <div className="flex items-center space-x-4 mt-[8px]">
                    <CopyToClipboard
                      text={item.link}
                      onCopy={() => setCopied(true)}
                      onClick={() => success}
                    >
                      <Button type="primary">Copy</Button>
                    </CopyToClipboard>

                    <ShareGatewayDialog />
                  </div>
                )}
              </Card>
            </Col>
          </Row>
        );
      })}
    </>
  );
};

export default Payment;
