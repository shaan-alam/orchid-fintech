import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { WhatsAppOutlined } from "@ant-design/icons";
import ReactWhatsapp from "react-whatsapp";
import { Checkbox } from "../ui/checkbox";

const ShareGatewayDialog = () => {
  return (
    <Dialog>
      <DialogTrigger>
        <Button>Share</Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Share Gateway Link</DialogTitle>
        </DialogHeader>
        <div className="flex items-center space-x-4">
          <div>
            <label htmlFor="" className="text-sm text-gray-600 mb-4 block">
              Gateway Link
            </label>
            <Button variant="secondary">
              <ReactWhatsapp
                number="7715815877"
                message="https://rzp.io/i/vOCA2rx2d3"
              />
              <WhatsAppOutlined style={{ color: "green", size: "80px" }} />
            </Button>
          </div>
          <div>
            <label
              htmlFor=""
              className="flex items-center space-x-2 mt-8"
            >
              <Checkbox />
              <p className="ml-2">Email</p>
            </label>
          </div>
        </div>
        <DialogFooter>
          <DialogClose>
            <Button variant='secondary'>Close</Button>
          </DialogClose>
          <Button>Submit</Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default ShareGatewayDialog;
