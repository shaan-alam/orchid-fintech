import { Button } from "@/components/ui/button";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Card, Col, Row } from "antd";
import { useState } from "react";
import ClientAxixs from "../../assets/ClientAxixs.jpeg";

const BankQR = () => {
  const bankNames = [
    { value: "FINNACLEENTERPRISES", label: "FINNACLE ENTERPRISES" },
  ];
  const [bank, setBank] = useState("FINNACLE ENTERPRISES");
  const downloadImage = () => {
    saveAs(ClientAxixs, "QR-CODE.jpg"); // Put your image url here.
  };
  const [showImage, setshowImage] = useState(false);

  const generate = () => {
    alert("QR Created Succefully");
    setshowImage(true);
  };

  return (
    <>
      <div className="rounded-xl bg-white p-4 shadow my-4">
        <div className="flex items-center space-x-2">
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              QR Code
            </label>
            <Select>
              <SelectTrigger className="w-[180px]">
                <SelectValue placeholder="QR Code" />
              </SelectTrigger>
              <SelectContent>
                {bankNames.map((bank) => (
                  <SelectItem value={bank.value}>{bank.label}</SelectItem>
                ))}
              </SelectContent>
            </Select>
          </div>
          <Button onClick={generate} className="mt-5">
            Generate QR
          </Button>
        </div>
        {!showImage ? (
          <></>
        ) : (
          <>
            <Row>
              <Col md="6"></Col>
              <Col md="6" style={{ textAlign: "center" }}>
                <br></br>
                <Button type="primary" onClick={downloadImage} className='my-4'>
                  Download QR
                </Button>
                <Card
                  style={{
                    textAlign: "center",
                    width: 250,
                  }}
                  cover={<img height={500} alt="example" src={ClientAxixs} />}
                >
                  {/* <img src={QrCode} size="50px"></img> */}
                </Card>
              </Col>
              <Col></Col>
            </Row>
          </>
        )}
      </div>
    </>
  );
};

export default BankQR;
