import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import { Input } from "../ui/input";
import BankDetails from "../BankDetails";
import BankQR from "../BankQR";
import Payment from "../Payment";
import Settings from "../Settings";

const Gateway = () => {
  return (
    <>
      <div className="flex items-center justify-between my-6">
        <h1 className="text-slate-900 font-semibold">Payment Transaction</h1>
        <div>
          <Input placeholder="Search" />
        </div>
      </div>
      <Tabs defaultValue="bank-details">
        <TabsList>
          <TabsTrigger value="bank-details">Bank Details</TabsTrigger>
          <TabsTrigger value="qr-code">QR Code</TabsTrigger>
          <TabsTrigger value="payment-link">Payment Link</TabsTrigger>
          <TabsTrigger value="settings">Settings</TabsTrigger>
        </TabsList>
        <TabsContent value="bank-details">
          <BankDetails />
        </TabsContent>
        <TabsContent value="qr-code">
          <BankQR />
        </TabsContent>
        <TabsContent value="payment-link">
          <Payment />
        </TabsContent>
        <TabsContent value="settings">
          <Settings />
        </TabsContent>
      </Tabs>
    </>
  );
};

export default Gateway;
