import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Button } from "../ui/button";
import { Input } from "@/components/ui/input";
import { cn } from "@/lib/utils";

const ConnectDialog = ({ connected }) => {
  return (
    <Dialog>
      <DialogTrigger>
        <Button className={cn(connected ? "bg-green-500" : "")}>
          {connected ? "Connected" : "Connect"}
        </Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Add Payment Gateway</DialogTitle>
        </DialogHeader>
        <div className="grid grid-cols-2 gap-4">
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Bank Name
            </label>
            <Input placeholder="Enter Bank Name" />
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Account No
            </label>
            <Input placeholder="Enter Account No" />
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              IFSC Code
            </label>
            <Input placeholder="Enter IFSC Code" />
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Branch Name
            </label>
            <Input placeholder="Enter Branch Name" />
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Account Holder Name
            </label>
            <Input placeholder="Enter Account Holder Name" />
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Mobile No
            </label>
            <Input placeholder="Enter Mobile No" />
          </div>
        </div>
        <div className="">
          <h1 className="text-primary font-semibold">Gateway's Credentials</h1>
        </div>
        <div className="grid grid-cols-2 gap-x-4">
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Merchant ID
            </label>
            <Input placeholder="Enter Merchant ID" />
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Merchant Key
            </label>
            <Input placeholder="Enter Merchant Key" />
          </div>
        </div>
        <DialogFooter>
          <DialogClose>
            <Button variant="secondary">Close</Button>
          </DialogClose>
          <Button>Submit</Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default ConnectDialog;
