import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Col, Descriptions, Row } from "antd";
import { Button } from "@/components/ui/button";
import { useState } from "react";

const ManagePaymentButton = () => {
  const [showForm, setShowForm] = useState(false);
  const [mobile, setMobile] = useState("");
  const [mesaage, setMessage] = useState(
    "Please Pay the Amount through this link !"
  );
  const [payLink, setPayLink] = useState("http://payment.finacle.co.in/");
  const [showImage, setshowImage] = useState(false);
  const [size, setSize] = useState("default");
  const [bankName, setBankName] = useState("AXIS BANK");

  const FINNACLEENTERPRISES = {
    accountNo: 922020041768621,
    accountHolderName: "Babu Ram Sarma",
    branchName: "Lal Ganesh, Guwahati",
    ifsc: "UTIB0002965",
    registeredMobile: 7983479210,
    email: "finnacleenterprises@gmail.com",
  };

  const ICICIBANK = {
    name: "ICICI BANK",
    accountNo: "026805007001",
    accountHolderName: "FINNACLE ENTERPRISES",
    branchName: "Thane - Vartak Nagar",
    ifsc: "ICIC0000268",
    registeredMobile: 7983479210,
    email: "finnacleenterprises@gmail.com",
  };
  const HDFCBANK = {
    name: "HDFC BANK",
    accountNo: "50200053168565",
    accountHolderName: "FINNACLE ENTERPRISES",
    branchName: "MUMBAI - KANDIVALI (EAST)",
    ifsc: "HDFC0000182",
    registeredMobile: 7983479210,
    email: "finnacleenterprises@gmail.com",
  };
  const IDFCBANK = {
    name: "IDFC BANK",
    accountNo: "10089669622",
    accountHolderName: "FINNACLE",
    branchName: "NAVI MUMBAI-AIROLI",
    ifsc: "IDFB0040152",
    registeredMobile: 7983479210,
    email: "finnacleenterprises@gmail.com",
  };
  const ICICI = {
    name: "ICICI BANK",
    accountNo: "054305501246",
    accountHolderName: "TRADE CAFE",
    branchName: "GUWAHATI - FANCY BAZAR",
    ifsc: "ICIC0000543",
    registeredMobile: 7983479210,
    email: "-",
  };

  const bankNames = [
    { value: "Select the Bank", label: "Select the Bank" },
    { value: "AXIS BANK", label: "AXIS BANK" },
    { value: "ICICI BANK", label: "ICICI BANK - Thane" },
    { value: "IDFC BANK", label: "IDFC BANK" },
    { value: "ICICI", label: "IDFC BANK - Fancy Bazar" },
    { value: "HDFC BANK", label: "HDFC BANK" },
  ];
  const whatsapp = () => {
    let url = `https://web.whatsapp.com/send?phone=${mobile}&text=${mesaage}%0a%0a${payLink}&app_absent=0`;

    window.open(url);
  };
  const sendLink = () => {
    handleClose();
    alert("Link Send Successfully!");
  };
  const getButton = () => {
    alert("Payment Button Created Succefully");
    const rzpPaymentForm = document.getElementById("rzp_payment_form");

    if (!rzpPaymentForm.hasChildNodes()) {
      const script = document.createElement("script");
      script.src = "https://checkout.razorpay.com/v1/payment-button.js";
      script.async = true;
      script.dataset.payment_button_id = "pl_LGeTghOkNTWnxm";
      rzpPaymentForm.appendChild(script);
    }
    console.log(rzpPaymentForm);
    data = rzpPaymentForm;
  };
  //   useEffect(() => {
  //     getButton();
  //   });

  const showModal1 = () => {
    setShowForm(true);
  };

  const handleOpen = () => {
    setShowForm(false);
  };

  const handleClose = () => {
    setShowForm(false);
  };

  return (
    <>
      <div className="rounded-xl bg-white shadow p-4">
        <div>
          <label htmlFor="" className="text-sm text-gray-600">
            Select Bank
          </label>
          <Select
            defaultValue={bankName}
            onValueChange={(value) => setBankName(value)}
          >
            <SelectTrigger className="w-[200px]">
              <SelectValue placeholder="Select Bank" />
            </SelectTrigger>
            <SelectContent>
              {bankNames.map((bank) => (
                <SelectItem value={bank.value}>{bank.label}</SelectItem>
              ))}
            </SelectContent>
          </Select>
        </div>

        <div>
          <br></br>
          {showImage ? (
            <>fdf</>
          ) : (
            <>
              {(() => {
                if (bankName == "AXIS BANK") {
                  return (
                    <>
                      <Descriptions
                        bordered
                        title="Bank Detail's"
                        size={size}
                        column={1}
                        // extra={<Button type="primary">Edit</Button>}
                      >
                        <Descriptions.Item label="Bank Name">
                          Axis Bank
                        </Descriptions.Item>
                        <Descriptions.Item label="Name">
                          {FINNACLEENTERPRISES.accountHolderName}
                        </Descriptions.Item>
                        <Descriptions.Item label="Account No" span={3}>
                          {FINNACLEENTERPRISES.accountNo}
                        </Descriptions.Item>
                        <Descriptions.Item label="Branch Name">
                          {FINNACLEENTERPRISES.branchName}
                        </Descriptions.Item>
                        <Descriptions.Item label="IFSC ">
                          {FINNACLEENTERPRISES.ifsc}
                        </Descriptions.Item>
                        <Descriptions.Item label="Registered Mobile">
                          {FINNACLEENTERPRISES.registeredMobile}
                        </Descriptions.Item>
                        <Descriptions.Item label="Registered Email">
                          {FINNACLEENTERPRISES.email}
                          <br></br>
                        </Descriptions.Item>
                      </Descriptions>
                      <br></br>
                      <div>
                        <br></br>

                        <div style={{ textAlign: "center" }}>
                          <Row>
                            <Col>
                              <Button
                                type="primary"
                                success
                                style={{ marginRight: "8px" }}
                                onClick={showModal1}
                              >
                                Share Button Link
                              </Button>
                            </Col>
                            <Col>
                              <Button type="primary" onClick={getButton}>
                                Generate Button{" "}
                              </Button>
                            </Col>
                            &nbsp;&nbsp;
                            <Col>
                              <form id="rzp_payment_form"></form>
                            </Col>
                          </Row>
                        </div>
                      </div>
                    </>
                  );
                } else if (bankName == "ICICI BANK") {
                  return (
                    <>
                      <Descriptions
                        bordered
                        title="Bank Detail's"
                        size={size}
                        column={1}
                        // extra={<Button type="primary">Edit</Button>}
                      >
                        <Descriptions.Item label="Bank Name">
                          {ICICIBANK.name}
                        </Descriptions.Item>
                        <Descriptions.Item label="Name">
                          {ICICIBANK.accountHolderName}
                        </Descriptions.Item>
                        <Descriptions.Item label="Account No" span={3}>
                          {ICICIBANK.accountNo}
                        </Descriptions.Item>
                        <Descriptions.Item label="Branch Name">
                          {ICICIBANK.branchName}
                        </Descriptions.Item>
                        <Descriptions.Item label="IFSC ">
                          {ICICIBANK.ifsc}
                        </Descriptions.Item>
                        <Descriptions.Item label="Registered Mobile">
                          {ICICIBANK.registeredMobile}
                        </Descriptions.Item>
                        <Descriptions.Item label="Registered Email">
                          {FINNACLEENTERPRISES.email}
                          <br></br>
                        </Descriptions.Item>
                      </Descriptions>
                    </>
                  );
                } else if (bankName == "HDFC BANK") {
                  return (
                    <>
                      <Descriptions
                        bordered
                        title="Bank Detail's"
                        size={size}
                        column={1}
                        // extra={<Button type="primary">Edit</Button>}
                      >
                        <Descriptions.Item label="Bank Name">
                          {HDFCBANK.name}
                        </Descriptions.Item>
                        <Descriptions.Item label="Name">
                          {HDFCBANK.accountHolderName}
                        </Descriptions.Item>
                        <Descriptions.Item label="Account No" span={3}>
                          {HDFCBANK.accountNo}
                        </Descriptions.Item>
                        <Descriptions.Item label="Branch Name">
                          {HDFCBANK.branchName}
                        </Descriptions.Item>
                        <Descriptions.Item label="IFSC ">
                          {HDFCBANK.ifsc}
                        </Descriptions.Item>
                        <Descriptions.Item label="Registered Mobile">
                          {HDFCBANK.registeredMobile}
                        </Descriptions.Item>
                        <Descriptions.Item label="Registered Email">
                          {HDFCBANK.email}
                          <br></br>
                        </Descriptions.Item>
                      </Descriptions>
                    </>
                  );
                } else if (bankName == "IDFC BANK") {
                  return (
                    <>
                      <Descriptions
                        bordered
                        title="Bank Detail's"
                        size={size}
                        column={1}
                        // extra={<Button type="primary">Edit</Button>}
                      >
                        <Descriptions.Item label="Bank Name">
                          {IDFCBANK.name}
                        </Descriptions.Item>
                        <Descriptions.Item label="Name">
                          {IDFCBANK.accountHolderName}
                        </Descriptions.Item>
                        <Descriptions.Item label="Account No" span={3}>
                          {IDFCBANK.accountNo}
                        </Descriptions.Item>
                        <Descriptions.Item label="Branch Name">
                          {IDFCBANK.branchName}
                        </Descriptions.Item>
                        <Descriptions.Item label="IFSC ">
                          {IDFCBANK.ifsc}
                        </Descriptions.Item>
                        <Descriptions.Item label="Registered Mobile">
                          {IDFCBANK.registeredMobile}
                        </Descriptions.Item>
                        <Descriptions.Item label="Registered Email">
                          {IDFCBANK.email}
                          <br></br>
                        </Descriptions.Item>
                      </Descriptions>
                    </>
                  );
                } else if (bankName == "ICICI") {
                  return (
                    <>
                      <Descriptions
                        bordered
                        title="Bank Detail's"
                        size={size}
                        column={1}
                        // extra={<Button type="primary">Edit</Button>}
                      >
                        <Descriptions.Item label="Bank Name">
                          {ICICI.name}
                        </Descriptions.Item>
                        <Descriptions.Item label="Name">
                          {ICICI.accountHolderName}
                        </Descriptions.Item>
                        <Descriptions.Item label="Account No" span={3}>
                          {ICICI.accountNo}
                        </Descriptions.Item>
                        <Descriptions.Item label="Branch Name">
                          {ICICI.branchName}
                        </Descriptions.Item>
                        <Descriptions.Item label="IFSC ">
                          {ICICI.ifsc}
                        </Descriptions.Item>
                        <Descriptions.Item label="Registered Mobile">
                          {ICICI.registeredMobile}
                        </Descriptions.Item>
                        <Descriptions.Item label="Registered Email">
                          {ICICI.email}
                          <br></br>
                        </Descriptions.Item>
                      </Descriptions>
                    </>
                  );
                } else {
                  return (
                    <>
                      <h3 style={{ textAlign: "center" }}>
                        Payment Gateway is not Connected
                      </h3>
                    </>
                  );
                }
              })()}
            </>
          )}{" "}
        </div>
      </div>
    </>
  );
};

export default ManagePaymentButton;
