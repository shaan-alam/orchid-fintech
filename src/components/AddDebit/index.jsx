import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DialogClose,
  DialogFooter,
} from "@/components/ui/dialog";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Button } from "../ui/button";
import { Input } from "@/components/ui/input";

const AddDebit = () => {
  const manager = [
    { value: "Vinod", label: "Vinod" },
    { value: "Rajeshwar", label: "Rajeshwar" },
    { value: "Vishal", label: "Vishal" },
  ];
  const location = [
    { value: "Thane", label: "Thane" },
    { value: "Juhu", label: "Juhu" },
    { value: "Kalyan", label: "Kalyan" },
  ];
  const bankName = [
    { value: "ICICI", label: "ICICI" },
    { value: "SBI", label: "SBI" },
    { value: "HDFC", label: "HDFC" },
    { value: "Kotak", label: "Kotak" },
  ];
  const dateFormat = "YYYY-MM-DD";
  const modes = [
    { value: "Cash", label: "Cash" },
    { value: "Cheque", label: "Cheque" },
    { value: "Debit Card", label: "Debit Card" },
  ];
  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <>
      <Dialog>
        <DialogTrigger>
          <Button>Add</Button>
        </DialogTrigger>
        <DialogContent>
          <DialogHeader>
            <DialogTitle>Add Debit</DialogTitle>
          </DialogHeader>
          <div className="grid grid-cols-2 gap-4">
            <div>
              <label htmlFor="" className="text-sm text-gray-600">
                Branch Manager
              </label>
              <Select>
                <SelectTrigger>
                  <SelectValue placeholder="Select Branch Manager" />
                </SelectTrigger>
                <SelectContent>
                  {manager.map((manager) => (
                    <SelectItem value={manager.value}>
                      {manager.label}
                    </SelectItem>
                  ))}
                </SelectContent>
              </Select>
            </div>
            <div>
              <label htmlFor="" className="text-sm text-gray-600">
                Branch Location
              </label>
              <Select>
                <SelectTrigger>
                  <SelectValue placeholder="Select Branch Location" />
                </SelectTrigger>
                <SelectContent>
                  {location.map((location) => (
                    <SelectItem value={location.value}>
                      {location.label}
                    </SelectItem>
                  ))}
                </SelectContent>
              </Select>
            </div>
            <div>
              <label htmlFor="" className="text-sm text-gray-600">
                Bank Name
              </label>
              <Select>
                <SelectTrigger>
                  <SelectValue placeholder="Select Bank Name" />
                </SelectTrigger>
                <SelectContent>
                  {bankName.map((bank) => (
                    <SelectItem value={bank.value}>{bank.label}</SelectItem>
                  ))}
                </SelectContent>
              </Select>
            </div>
            <div>
              <label htmlFor="" className="text-sm text-gray-600">
                Payment Gateway
              </label>
              <Select>
                <SelectTrigger>
                  <SelectValue placeholder="Payment Gateway" />
                </SelectTrigger>
                <SelectContent>
                  {modes.map((gateway) => (
                    <SelectItem value={gateway.value}>
                      {gateway.label}
                    </SelectItem>
                  ))}
                </SelectContent>
              </Select>
            </div>
            <div>
              <label htmlFor="" className="text-sm text-gray-600">
                Total Amount
              </label>
              <Input placeholder="Enter total amount" />
            </div>
            <div>
              <label htmlFor="" className="text-sm text-gray-600">
                Gateway Charges
              </label>
              <Input placeholder="Enter Gateway Charges" />
            </div>
            <div>
              <label htmlFor="" className="text-sm text-gray-600">
                Commission
              </label>
              <Input placeholder="Enter Commission Amount" />
            </div>
            <div>
              <label htmlFor="" className="text-sm text-gray-600">
                Settlement Amount
              </label>
              <Input placeholder="Enter Settlement Amount" />
            </div>
          </div>
          <DialogFooter>
            <DialogClose>
              <Button variant="secondary">Close</Button>
            </DialogClose>
            <Button>Add</Button>
          </DialogFooter>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default AddDebit;
