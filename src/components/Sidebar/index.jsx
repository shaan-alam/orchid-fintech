import BillingIcon from "@/icons/BillingIcon";
import DashboardIcon from "@/icons/DashboardIcon";

import { cn } from "@/lib/utils";
import { Link, useLocation } from "react-router-dom";
import { HiLogout } from "react-icons/hi";

const Sidebar = () => {
  const { pathname } = useLocation();

  const links = [
    { text: "Dashboard", icon: DashboardIcon, path: "/dashboard" },
    {
      text: "Payment Product",
      icon: BillingIcon,
      path: "/payment-product",
    },
    {
      text: "Payment Transaction",
      icon: DashboardIcon,
      path: "/payment-transaction",
    },
    { text: "Bank Integration", icon: BillingIcon, path: "/gateway" },
  ];

  return (
    <aside className="border-r h-screen fixed w-[15%] bg-[#2D3736]">
      <div className="logo pb-4 flex items-center space-x-4 relative z-20 bg-[#D84349] p-4">
        <img src="/logo.jpeg" alt="" className="w-[8%]" />
        <h1 className="text-white font-bold text-lg">Orchid Fintech</h1>
      </div>
      <div className="relative z-10 p-4">
        <ul>
          {links.map((link) => (
            <Link to={link.path}>
              <li
                className={cn(
                  "flex items-center space-x-4 mt-4 px-4 py-3 text-white hover:bg-white hover:bg-opacity-50 bg-opacity-50 rounded-lg",
                  pathname === link.path
                    ? "bg-white [&>span]:text-white"
                    : "hover:bg-white"
                )}
              >
                <span>
                  <link.icon color={pathname === link.path ? "#fff" : "#fff"} />
                </span>
                <span className="font-medium">{link.text}</span>
              </li>
            </Link>
          ))}
          <Link to="/">
            <li
              className={cn(
                "flex items-center space-x-4 mt-4 px-4 py-3 text-white hover:bg-white hover:bg-opacity-50 bg-opacity-50 rounded-lg"
              )}
            >
              <span>
                <HiLogout />
              </span>
              <span className="font-medium">Sign Out</span>
            </li>
          </Link>
        </ul>
      </div>
    </aside>
  );
};

export default Sidebar;
