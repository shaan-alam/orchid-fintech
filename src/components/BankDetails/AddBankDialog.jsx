import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DialogClose,
  DialogFooter,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";

const AddBankDialog = () => {
  return (
    <Dialog>
      <DialogTrigger>
        <Button>Add Bank</Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Add Bank</DialogTitle>
        </DialogHeader>
        <div className="grid grid-cols-2 gap-4">
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Bank Name
            </label>
            <Input placeholder="Enter Bank Name" />
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Account No
            </label>
            <Input placeholder="Enter Account No" />
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              IFSC Code
            </label>
            <Input placeholder="Enter IFSC Code" />
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Branch name
            </label>
            <Input placeholder="Enter Branch Name" />
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Account Holder Name
            </label>
            <Input placeholder="Enter Account Holder Name" />
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600">
              Mobile No
            </label>
            <Input placeholder="Enter Mobile No" />
          </div>
        </div>
        <DialogFooter>
          <DialogClose>
            <Button variant="secondary">Close</Button>
          </DialogClose>
          <Button>Add</Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default AddBankDialog;
