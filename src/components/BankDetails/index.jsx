import { Input } from "@/components/ui/input";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Card, Col, Descriptions, Row } from "antd";
import { useState } from "react";
import AddBankDialog from "./AddBankDialog";

const BankDetails = () => {
  const [bankName, setBankName] = useState("FINNACLEENTERPRISES");

  const bankNames = [
    { value: "FINNACLEENTERPRISES", label: "FINNACLE ENTERPRISES" },
    { value: "ICICI", label: "ICICI" },
    { value: "Bank Of Maharashtra", label: "Bank Of Maharashtra" },
  ];
  const FINNACLEENTERPRISES = {
    accountNo: 922020041768621,
    accountHolderName: "Babu Ram Sarma",
    branchName: "Lal Ganesh, Guwahati",
    ifsc: "UTIB0002965",
    registeredMobile: 7983479210,
    email: "finnacleenterprises@gmail.com",
  };
  // const ICICI = {
  //   accountNo: 2545445453362,
  //   accountHolderName: "ORCHID FINTECH",
  //   branchName: "Gansoli",
  //   ifsc: "ICICI5412",
  //   registeredMobile: 95632874125,
  //   email: "orchidfintech@yahoo.com",
  // };
  const [size, setSize] = useState("default");

  return (
    <>
      <div className="rounded-xl bg-white p-4 shadow flex items-center space-x-2 my-4">
        <Input placeholder="Search" />
        <AddBankDialog />
      </div>
      <div className="rounded-xl bg-white p-4 shadow">
        <div className="flex items-center justify-between">
          <h1>Bank Details</h1>
          <div className="flex items-center space-x-2">
            <label htmlFor="" className="text-gray-600 text-sm">
              Select Bank:
            </label>
            <Select onValueChange={(value) => setBankName(value)}>
              <SelectTrigger className="w-[220px]">
                <SelectValue placeholder="Bank" />
              </SelectTrigger>
              <SelectContent>
                {bankNames.map((bank) => (
                  <SelectItem value={bank.value}>{bank.label}</SelectItem>
                ))}
              </SelectContent>
            </Select>
          </div>
        </div>
        <div className="tabled" style={{ marginTop: "30px" }}>
          <Row gutter={[24, 0]}>
            <Col xs="24" xl={24}>
              <Card
                bordered={true}
                className="criclebox tablespace mb-24"
                title={
                  <>
                    <Row
                      gutter={[24, 0]}
                      className="ant-row-flex ant-row-flex-middle"
                    >
                      <Col xs={24} md={10} style={{ textAlign: "end" }}></Col>
                      <Col xs={24} md={10}>
                        <Select
                          className="font-semibold m-0"
                          options={bankNames}
                          value={bankName}
                          size="large"
                          onChange={(e) => {
                            setBankName(e);
                          }}
                          placeholder="Select Bank"
                          style={{ width: "200px" }}
                        ></Select>
                      </Col>
                      {/* <Col xs={24} md={4} className="d-flex">
                      <Button type="primary" onClick={showModal}>
                        ADD
                      </Button>
                    </Col> */}
                    </Row>
                    {bankName == "FINNACLEENTERPRISES" ? (
                      <>
                        {" "}
                        <Descriptions
                          bordered
                          size={size}
                          // extra={<Button type="primary">Edit</Button>}
                        >
                          <Descriptions.Item label="Bank Name">
                            State Bank of India
                          </Descriptions.Item>
                          <Descriptions.Item label="Name">
                            {FINNACLEENTERPRISES.accountHolderName}
                          </Descriptions.Item>
                          <Descriptions.Item label="Account No" span={3}>
                            {FINNACLEENTERPRISES.accountNo}
                          </Descriptions.Item>
                          <Descriptions.Item label="Branch Name">
                            {FINNACLEENTERPRISES.branchName}
                          </Descriptions.Item>
                          <Descriptions.Item label="IFSC ">
                            {FINNACLEENTERPRISES.ifsc}
                          </Descriptions.Item>
                          <Descriptions.Item label="Registered Mobile">
                            {FINNACLEENTERPRISES.registeredMobile}
                          </Descriptions.Item>
                          <Descriptions.Item label="Registered Email">
                            {FINNACLEENTERPRISES.email}
                            <br></br>
                          </Descriptions.Item>
                        </Descriptions>
                        <br></br>
                        <Descriptions bordered size={size}>
                          <Descriptions.Item label="Merchant Key">
                            dgfd@4gmkt555k3414gfgfdgeweffd
                          </Descriptions.Item>

                          <Descriptions.Item label="Merchant Id">
                            5422gbggfdnj3#
                          </Descriptions.Item>
                        </Descriptions>
                      </>
                    ) : (
                      <></>
                    )}
                    {/* {bankName == "ICICI" ? (
                    <>
                      {" "}
                      <Descriptions
                        bordered
                        title="Bank Detail's"
                        size={size}
                        // extra={
                        //   <Button type="primary" onClick={showModal}>
                        //     Edit
                        //   </Button>
                        // }
                      >
                        <Descriptions.Item label="Bank Name">
                          ICICI Bank
                        </Descriptions.Item>
                        <Descriptions.Item label="Name">
                          {ICICI.accountHolderName}
                        </Descriptions.Item>
                        <Descriptions.Item label="Account No" span={3}>
                          {ICICI.accountNo}
                        </Descriptions.Item>
                        <Descriptions.Item label="Branch Name">
                          {ICICI.branchName}
                        </Descriptions.Item>
                        <Descriptions.Item label="IFSC ">
                          {ICICI.ifsc}
                        </Descriptions.Item>
                        <Descriptions.Item label="Registered Mobile">
                          {ICICI.registeredMobile}
                        </Descriptions.Item>
                        <Descriptions.Item label="Registered Email">
                          {ICICI.email}
                          <br></br>
                        </Descriptions.Item>
                      </Descriptions>
                      <br></br>
                      <Descriptions bordered size={size}>
                        <Descriptions.Item label="Merchant Key">
                          dgfd@4gmkt555k3414gfgfdgeweffd
                        </Descriptions.Item>

                        <Descriptions.Item label="Merchant Id">
                          5422gbggfdnj3#
                        </Descriptions.Item>
                      </Descriptions>
                    </>
                  ) : (
                    <></>
                  )} */}
                  </>
                }
              >
                {/* <div className="table-responsive">
                <Table
                  columns={columns}
                  dataSource={data}
                  pagination={false}
                  className="ant-border-space"
                />
              </div> */}
              </Card>

              {/* <Card
                bordered={false}
                className="criclebox tablespace mb-24"
                title="Projects Table"
                extra={
                  <>
                    <Radio.Group onChange={onChange} defaultValue="all">
                      <Radio.Button value="all">All</Radio.Button>
                      <Radio.Button value="online">ONLINE</Radio.Button>
                      <Radio.Button value="store">STORES</Radio.Button>
                    </Radio.Group>
                  </>
                }
              >
                <div className="table-responsive">
                  <Table
                    columns={project}
                    dataSource={dataproject}
                    pagination={false}
                    className="ant-border-space"
                  />
                </div>
                <div className="uploadfile pb-15 shadow-none">
                  <Upload {...formProps}>
                    <Button
                      type="dashed"
                      className="ant-full-box"
                      icon={<ToTopOutlined />}
                    >
                      Click to Upload
                    </Button>
                  </Upload>
                </div>
              </Card>*/}
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
};

export default BankDetails;
