import { useEffect, useState } from "react";
// import { Chip } from "antd";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import Axios from "axios";
import { BASE_URL } from "@/utils/constants";
import { Button } from "../ui/button";
import { DataTable } from "../ui/data-table";
import { columns } from "./columns";

const QRTransactions = () => {
  const [APIData, setAPIData] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [showForm, setShowForm] = useState(false);
  const [obj, setObj] = useState(null);

  const handleshow = (row) => {
    setObj(row);
    setShowForm(true);
  };

  const handleClose = () => {
    setShowForm(false);
    getData();
  };
  const [id, setID] = useState([0]);
  const fetchQrId = async () => {
    await Axios.get(BASE_URL + "/all_qr_codes").then((res) => {
      setID(res.data.items);
      console.log(id);
    });
  };

  const [qrId, setQrId] = useState("");

  const getData = async () => {
    await Axios.get(BASE_URL + "/qr_codes/" + qrId + "/payments").then(
      (res) => {
        setAPIData(
          res.data.items.map((row) => ({
            id: <a>{row.id}</a>,
            currency: row.currency,
            amount: (
              <>
                ₹ <span>{row.amount / 100}</span>
              </>
            ),
            method: row.method,
            wallet: row.wallet,
            email: row.email,
            contact: row.contact,
            status: (
              <>
                {row.status == "captured" ? (
                  <>
                    <Chip size="small" label="Captured" color="success" />
                  </>
                ) : (
                  <>
                    {" "}
                    <Chip size="small" label="Failed" color="warning" />
                  </>
                )}
              </>
            ),
            //   action: (
            //     <>
            //       <IconButton onClick={() => handleshow(row)}>
            //         <EyeOutlined style={{ fontSize: "18px" }}></EyeOutlined>
            //       </IconButton>
            //     </>
            //   ),
          }))
        );
      }
    );
    console.log({ qr_id: qrId });
  };
  useEffect(() => {
    getData();
    fetchQrId();
  }, []);

  return (
    <>
      <div className="my-4 rounded-xl bg-white shadow p-4">
        <label htmlFor="" className="text-sm text-gray-600">
          Select QR ID
        </label>
        <div className="flex items-center space-x-2">
          <Select>
            <SelectTrigger className="w-[200px]">
              <SelectValue placeholder="QR Code ID" />
            </SelectTrigger>
            <SelectContent>
              {id.map((l) => {
                return (
                  <SelectItem key={l.id} value={l.id}>
                    {l.id}
                  </SelectItem>
                );
              })}
            </SelectContent>
          </Select>
          <Button>Search</Button>
        </div>
      </div>
      <div className="my-4 rounded-xl bg-white shadow p-4">
        {APIData && <DataTable columns={columns} data={APIData} />}
      </div>
    </>
  );
};

export default QRTransactions;
