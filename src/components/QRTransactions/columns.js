export const columns = [
  {
    accessorKey: "id",
    header: "Payment Link ID",
  },
  {
    accessorKey: "currency",
    header: "Currency",
  },
  {
    accessorKey: "method",
    header: "Method",
  },
  {
    accessorKey: "wallet",
    header: "Wallet",
  },
  {
    accessorKey: "email",
    header: "Email",
  },
  {
    accessorKey: "contact",
    header: "Contact",
  },
  {
    accessorKey: "status",
    header: "Status",
  },
];
