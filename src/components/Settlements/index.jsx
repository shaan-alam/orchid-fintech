const Settlements = () => {
  return (
    <>
      <div className="grid grid-cols-3 gap-4 mt-4">
        <div className="p-4 border-r">
          <h1 className="text-slate-700 font-semibold text-2xl mb-4">
            Current Balance
          </h1>
          <p className="text-xl font-medium text-gray-500">₹0.00</p>
        </div>
        <div className="p-4 border-r">
          <h1 className="text-slate-700 font-semibold text-2xl mb-4">
            Settlement due today
          </h1>
          <div className="flex items-center space-x-24">
            <p className="text-xl font-medium text-gray-500">₹3,909.50</p>
            <span className="bg-red-100 text-red-500 p-1 px-2 text-xs rounded-full">₹3,909.50 delayed</span>
          </div>
          <p className="text-sm text-gray-600 my-8">2 Settlements processed by 30 Jul, 4:05 PM</p>
        </div>
        <div className="p-4 border-r">
          <h1 className="text-slate-700 font-semibold text-2xl mb-4">
            Current Balance
          </h1>
          <p className="text-xl font-medium text-gray-500">₹0.00</p>
        </div>
      </div>
    </>
  );
};

export default Settlements;
