import { Button } from "../ui/button";
import { DataTable } from "../ui/data-table";
import CreatePaymentLinkDialog from "./CreatePaymentLinkDialog";
import { Input } from "@/components/ui/input";
import { columns } from "./columns";
import { Table } from "antd";
import { useEffect, useState } from "react";

const PaymentLink = () => {
  const [APIData, setAPIData] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [showForm, setShowForm] = useState(false);
  const [obj, setObj] = useState(null);

  const columns = [
    {
      title: "Payment Link Id",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Currency",
      dataIndex: "currency",
      key: "currency",
    },
    {
      title: "Amount",
      dataIndex: "amount",
      key: "amount",
    },
    {
      title: "Customer",
      dataIndex: "customer",
      key: "customer",
    },

    {
      title: "Payment Link",
      dataIndex: "short_url",
      key: "short_url",
    },
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
    },
    {
      title: "Action",
      key: "action",
      dataIndex: "action",
    },
  ];

  const handleshow = (row) => {
    setObj(row);
    setShowForm(true);
  };

  const handleClose = () => {
    setShowForm(false);
    getData();
  };

  const [showForm1, setShowForm1] = useState(false);
  const [obj1, setObj1] = useState(null);
  const handleNotify = (row) => {
    setObj1(row);
    setShowForm1(true);
  };

  const CloseNotify = () => {
    setShowForm1(false);
    getData();
  };

  const getData = async () => {
    await Axios.get(BASE_URL + "/payment_links/").then((res) => {
      setAPIData(
        res.data.payment_links.map((row) => ({
          id: row.id,
          currency: row.currency,
          amount: (
            <>
              ₹ <span>{row.amount / 100}</span>
            </>
          ),
          customer: row.customer.name,
          short_url: <a>{row.short_url}</a>,
          status: (
            <>
              {row.status == "created" ? (
                <>
                  <Chip size="small" label="Created" color="success" />
                </>
              ) : (
                <>
                  {" "}
                  <Chip size="small" label="Cancelled" color="warning" />
                </>
              )}
            </>
          ),
          action: (
            <>
              <IconButton onClick={() => handleshow(row)}>
                <EyeOutlined style={{ fontSize: "18px" }}></EyeOutlined>
              </IconButton>
              <IconButton onClick={() => handleNotify(row)}>
                <SendOutlined style={{ fontSize: "18px" }}></SendOutlined>
              </IconButton>
            </>
          ),
        }))
      );
    });
  };

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    getData();
  };

  useEffect(() => {
    getData();
  }, [APIData]);

  return (
    <>
      <div className="rounded-xl bg-white p-4 shadow">
        <div className="flex items-center space-x-2">
          <CreatePaymentLinkDialog />
          <Input placeholder="Search" />
        </div>
        <div className="grid grid-cols-6 gap-4 my-6">
          <div>
            <label htmlFor="" className="text-sm text-gray-600 mb-2 block">
              Payment Link Status
            </label>
            <Input placeholder="Status" />
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600 mb-2 block">
              Payment Link ID
            </label>
            <Input placeholder="Search By Payment ID" />
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600 mb-2 block">
              Customer Email
            </label>
            <Input placeholder="Search By Email" />
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600 mb-2 block">
              Customer Contact
            </label>
            <Input placeholder="Search By Phone" />
          </div>
          <div>
            <label htmlFor="" className="text-sm text-gray-600 mb-2 block">
              Count
            </label>
            <Input placeholder="Enter Limit" />
          </div>
          <div className="flex items-center space-x-2">
            <Button className="mt-6">Search</Button>
            <Button className="mt-6" variant="secondary">
              Cancel
            </Button>
          </div>
        </div>
        <div>
          <Table
            columns={columns}
            dataSource={APIData}
            pagination={false}
            rowKey="id"
            className="ant-border-space"
          />
        </div>
      </div>
    </>
  );
};

export default PaymentLink;
