export const columns = [
  {
    header: "Payment Link ID",
    accessorKey: "id",
  },
  {
    header: "Currency",
    accessorKey: "currency",
  },
  {
    header: "Amount",
    accessorKey: "amount",
    cell: () => {
      
    }
  },
  {
    header: "Customer",
    accessorKey: "customer",
  },
  {
    header: "Payment Link",
    accessorKey: "short_url",
  },
  {
    header: "Status",
    accessorKey: "status",
  },
  {
    header: "Actions",
    accessorKey: "actions",
    cell: () => {

    }
  },
];
