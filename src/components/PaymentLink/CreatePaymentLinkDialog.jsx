import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DialogFooter,
  DialogClose,
} from "@/components/ui/dialog";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Checkbox } from "../ui/checkbox";

const CreatePaymentLinkDialog = () => {
  return (
    <Dialog>
      <DialogTrigger>
        <Button>Create Payment Link</Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Payment Link</DialogTitle>
        </DialogHeader>
        <div>
          <div className="mb-4">
            <label htmlFor="" className="text-sm text-gray-600">
              Amount
            </label>
            <Input placeholder="Enter the amount" />
          </div>
          <div className="mb-4">
            <label htmlFor="" className="text-sm text-gray-600">
              Payment For
            </label>
            <Input placeholder="Enter the Payment description" />
          </div>
          <div className="mb-4">
            <label htmlFor="" className="text-sm text-gray-600">
              Customer Name
            </label>
            <Input placeholder="Enter the Customer Name" />
          </div>
          <div className="mb-4">
            <label htmlFor="" className="text-sm text-gray-600">
              Customer Phone
            </label>
            <Input placeholder="Enter the Customer Phone" />
          </div>
          <div className="flex items-center space-x-2 mb-4">
            <label htmlFor="" className="flex items-center">
              <Checkbox></Checkbox>
              <p className="ml-3">Notify via Email</p>
            </label>
            <label htmlFor="" className="flex items-center">
              <Checkbox></Checkbox>
              <p className="ml-3">Notify via SMS</p>
            </label>
          </div>
        </div>
        <DialogFooter>
          <DialogClose>
            <Button variant='secondary'>Close</Button>
          </DialogClose>
          <Button>Add Payment Link</Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default CreatePaymentLinkDialog;
