import { useEffect, useState } from "react";
import CreateQRCodeDialog from "./CreateQRCodeiDialog";
import { Input } from "../ui/input";
import { Button } from "../ui/button";
import { DataTable } from "../ui/data-table";
import { columns } from "./columns";

const ManageQrList = () => {
  const [APIData, setAPIData] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [showForm, setShowForm] = useState(false);
  const [obj, setObj] = useState(null);

  const handleshow = (row) => {
    setObj(row);
    setShowForm(true);
  };

  const handleClose = () => {
    getData();
    setShowForm(false);
  };

  const getData = async () => {
    await Axios.get(BASE_URL + "/all_qr_codes").then((res) => {
      setAPIData(
        res.data.items.map((row) => ({
          id: row.id,
          description: row.description,
          payments_amount_received: <>₹ {row.payments_amount_received}</>,
          // customer: row.customer.name,
          usage: row.usage,
          status: (
            <>
              {row.status == "active" ? (
                <>
                  <Chip size="small" label="Active" color="success" />
                </>
              ) : (
                <>
                  {" "}
                  <Chip size="small" label="Closed" color="warning" />
                </>
              )}
            </>
          ),
          action: (
            <>
              <IconButton onClick={() => handleshow(row)}>
                <EyeOutlined style={{ fontSize: "18px" }}></EyeOutlined>
              </IconButton>
            </>
          ),
        }))
      );
    });
  };

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    getData();
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    getData();
    setIsModalOpen(false);
  };

  useEffect(() => {
    getData();
  }, [APIData]);

  return (
    <div className="p-6 rounded-xl shadow bg-white">
      <div className="flex items-center space-x-2 my-6">
        <CreateQRCodeDialog />
        <Input placeholder="Search" />
      </div>
      <div className="grid grid-cols-6 gap-4 my-6">
        <div>
          <label htmlFor="" className="text-sm text-gray-600 mb-2 block">
            QR Code Status
          </label>
          <Input placeholder="Status" />
        </div>
        <div>
          <label htmlFor="" className="text-sm text-gray-600 mb-2 block">
            QR Code ID
          </label>
          <Input placeholder="Search By Payment ID" />
        </div>
        <div>
          <label htmlFor="" className="text-sm text-gray-600 mb-2 block">
            Customer Email
          </label>
          <Input placeholder="Search By Email" />
        </div>
        <div>
          <label htmlFor="" className="text-sm text-gray-600 mb-2 block">
            Customer Contact
          </label>
          <Input placeholder="Search By Phone" />
        </div>
        <div>
          <label htmlFor="" className="text-sm text-gray-600 mb-2 block">
            Note
          </label>
          <Input placeholder="Enter Limit" />
        </div>
        <div className="flex items-center space-x-2">
          <Button className="mt-6">Search</Button>
          <Button className="mt-6" variant="secondary">
            Cancel
          </Button>
        </div>
      </div>
      <div>
        <DataTable columns={columns} data={[]} />
      </div>
    </div>
  );
};

export default ManageQrList;
