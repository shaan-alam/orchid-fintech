import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DialogClose,
  DialogFooter,
} from "@/components/ui/dialog";
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group";
import { Input } from "@/components/ui/input";
import { useState } from "react";

const CreateQRCodeDialog = () => {
  const [fixed, setFixed] = useState(1);

  const [type, setType] = useState("upi_qr");
  const [name, setName] = useState("");
  const [paymentAmount, setPaymentAmount] = useState(1);
  const [description, setDescription] = useState("");
  const [purpose, setPurpose] = useState("");

  const [usage, setUsage] = useState("multiple_use");

  //Add data in the table
  const postData = () => {
    var check;
    if (fixed == 1) {
      check = false;
    } else {
      check = true;
    }
    const qr_data = {
      type: type,
      name: name,
      usage: usage,
      fixed_amount: check,
      payment_amount: paymentAmount * 100,
      description: description,
      close_by: 1981615838,
      notes: {
        purpose: purpose,
      },
    };
    Axios.post(BASE_URL + "/qr_codes", qr_data);
    console.log(qr_data);
    handleCancel();
  };
  const close = () => {
    handleCancel();
  };
  const checklist = [
    { label: "multiple_use", value: "Multiple Payment" },
    { label: "single_use", value: "Single Payment" },
  ];
  function handleChange(checkedValues) {
    setUsage(checkedValues.target.value);
  }

  return (
    <>
      <Dialog>
        <DialogTrigger>
          <Button>Create QR Code</Button>
        </DialogTrigger>
        <DialogContent className="max-w-[900px]">
          <DialogHeader>
            <DialogTitle>Create QR</DialogTitle>
          </DialogHeader>
          <div className="flex">
            <div className="w-1/2 p-4">
              <img src="/qr-code-img.jpeg" alt="" className="w-[90%] mx-auto" />
            </div>
            <div className="w-1/2">
              <div className="my-4">
                <label htmlFor="" className="text-sm text-gray-600 mb-2 block">
                  QR
                </label>
                <div className=" items-center space-x-4">
                  <RadioGroup
                    defaultValue="multiple-payment"
                    className="flex items-center space-x-2"
                  >
                    <div className="flex items-center space-x-2">
                      <RadioGroupItem
                        value="multiple-payment"
                        id="multiple-payment"
                      />
                      <label htmlFor="multiple-payment">Multiple Payment</label>
                    </div>
                    <div className="flex items-center space-x-2">
                      <RadioGroupItem
                        value="single-payment"
                        id="single-payment"
                      />
                      <label htmlFor="single-payment">Single Payment</label>
                    </div>
                  </RadioGroup>
                </div>
              </div>
              <div className="my-4">
                <label htmlFor="" className="text-sm text-gray-600 mb-2 block">
                  Accept only fixed amount on this QR?
                </label>
                <RadioGroup
                  defaultValue="no"
                  className="flex items-center space-x-2"
                  value={fixed}
                  onValueChange={(value) => setFixed(value)}
                >
                  <div className="flex items-center space-x-2">
                    <RadioGroupItem value={1} id={1} />
                    <label htmlFor="no">No</label>
                  </div>
                  <div className="flex items-center space-x-2">
                    <RadioGroupItem value={2} id={2} />
                    <label htmlFor="single-payment">Yes</label>
                  </div>
                </RadioGroup>
              </div>
              {fixed === 2 && (
                <>
                  <div className="my-4">
                    <label
                      htmlFor=""
                      className="text-sm text-gray-600 mb-2 block"
                    >
                      Amount
                    </label>
                    <Input placeholder="Enter fixed amount" />
                  </div>
                </>
              )}
              <div className="my-4">
                <label htmlFor="" className="text-sm text-gray-600 mb-2 block">
                  Descrpition (Optional)
                </label>
                <Input placeholder="Description will be visible in QR Code" />
              </div>
              <div className="my-4">
                <label htmlFor="" className="text-sm text-gray-600 mb-2 block">
                  QR Name (Optional)
                </label>
                <Input placeholder="QR Name will be visible in QR Code" />
              </div>
              <DialogFooter>
                <DialogClose>
                  <Button variant="secondary">Cancel</Button>
                </DialogClose>
                <Button>Add Payment Link</Button>
              </DialogFooter>
            </div>
          </div>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default CreateQRCodeDialog;
