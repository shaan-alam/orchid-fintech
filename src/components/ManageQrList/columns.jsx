export const columns = [
  {
    header: "QR Code Id",
    accessorKey: "id",
  },
  {
    header: "Description",
    accessorKey: "description",
  },
  {
    header: "QR Usage",
    accessorKey: "usage",
  },
  {
    header: "Amount Received",
    accessorKey: "payments_amount_received",
  },
  {
    header: "Status",
    accessorKey: "status",
  },
  {
    header: "Action",
    accessorKey: "action",
  },
];
